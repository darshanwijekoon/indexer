<?php

$start = microtime(true);
ini_set('memory_limit', '160M');

require 'vendor/autoload.php';

$dsn = sprintf('mysql:host=%s;dbname=%s;charset=%s;', 'mysql', 'wwwbase_eg', 'utf8');
$pdo = new PDO($dsn, 'root', 'root');
$pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);

printf('Querying locations...%s', PHP_EOL);

// Property count per location (size)
$stmt = $pdo->query('
    SELECT 
        location_id,
        COUNT(property_id) 
    FROM
        property 
    WHERE property_published = 1 
    GROUP BY location_id 
');
$sizes = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);

// All published locations
$stmt = $pdo->query('
    SELECT 
        * 
    FROM
        pf_location 
    WHERE deleted_at IS NULL 
        AND published = 1 
');
$locations = [];
while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
    // Defaults
    if (isset($sizes[$row->id])) {
        $row->size = (int)$sizes[$row->id];
    } else {
        $row->size = 0;
    }
    $row->children_count = 0;
    $locations[$row->id] = $row;
}

// Some aggregations
foreach ($locations as $location) {
    $path = explode('.', $location->path);
    $pathEn = [];
    $pathAr = [];
    // When it's deeper than city level
    if (count($path) > 1) {
        $reversePath = array_reverse($path);
        // Remove current location
        array_shift($reversePath);
        // Calculate sizes
        foreach ($reversePath as $parentId) {
            // Parent location
            if (isset($locations[$parentId])) {
                $locations[$parentId]->size += $location->size;
            }
        }
        // Increment parent's number of children
        foreach ($reversePath as $parentId) {
            if (isset($locations[$parentId])) {
                $locations[$parentId]->children_count++;
                break;
            }
        }
        // Estimate missing coordinates
        if ($location->coordinates_lat == 0 || $location->coordinates_lon == 0) {
            // Go up the tree looking for coordinates
            foreach ($reversePath as $parentId) {
                if (isset($locations[$parentId])) {
                    $parent = $locations[$parentId];
                    if ($parent->coordinates_lat > 0 && $parent->coordinates_lon > 0) {
                        $location->coordinates_lat = $parent->coordinates_lat;
                        $location->coordinates_lon = $parent->coordinates_lon;
                        break;
                    }
                }
            }
        }
        // Remove current location
        array_pop($path);
        // Concat path
        foreach ($path as $childId) {
            if (isset($locations[$childId])) {
                $child = $locations[$childId];
                $pathEn[] = $child->name_secondary;
                $pathAr[] = $child->name_primary;
            }
        }
    } else {
        // No path for cities
        $pathEn[] = '';
        $pathAr[] = '';
    }

    // Just make sure presence of coordinates
    if ($location->coordinates_lat == 0 || $location->coordinates_lon == 0) {
        throw new \Exception(sprintf('Could not find coordinates for location id [%s]', $location->id));
    }
    // Paths
    $location->path_en = implode(', ', $pathEn);
    $location->path_ar = implode(', ', $pathAr);
}

$elastic = \Elasticsearch\ClientBuilder::create()
    ->setHosts(['es'])
    ->build();

// Mapping
$params = [
    'index' => 'location-eg',
    'body' => [
        'settings' => [
            'number_of_shards' => 3,
            'mappings' => [
                'location' => [
                    'include_in_all' => false,
                    'properties' => [
                        'coordinates' => [
                            'type' => 'geo_point',
                            'lat_lon' => true,
                        ],
                    ],
                ],
            ],
        ],
    ],
];

printf('Deleting current location index...%s', PHP_EOL);

$elastic->indices()->delete(['index' => 'location-eg']);
// Create it!
$response = $elastic->indices()->create($params);
if (!$response['acknowledged']) {
    throw new \Exception('Error while creating index');
}

printf('Indexing location index...%s', PHP_EOL);

foreach (array_chunk($locations, 5000) as $chunk) {
    $params = ['body' => []];
    foreach ($chunk as $location) {
        $params['body'][] = [
            'index' => [
                '_index' => 'location-eg',
                '_type' => 'location',
                '_id' => $location->id,
            ],
        ];
        $params['body'][] = [
            'raw_id' => ($location->raw_id) ? (int)$location->raw_id : null,
            'name_ar' => $location->name_primary,
            'name_en' => $location->name_secondary,
            'coordinates' => [
                'lat' => (float)$location->coordinates_lat,
                'lon' => (float)$location->coordinates_lon,
            ],
            'address' => null,
            'published' => (int)$location->published,
            'abbreviation' => $location->abbreviation,
            'level' => (int)$location->level,
            'path_ar' => $location->path_ar,
            'path_en' => $location->path_en,
            'url_slug' => $location->slug_primary,
            'url_city_slug' => $location->url_city_slug,
            'size' => (int)$location->size,
            'path' => $location->deprecated_path,
            'changed_by' => ($location->changed_by) ? $location->changed_by : null,
            'doubleclick_key' => $location->doubleclick_key,
            'location_id' => (int)$location->id,
            'location_path' => $location->path,
            'name_primary' => $location->name_primary,
            'name_secondary' => $location->name_secondary,
            'slug_primary' => $location->slug_primary,
            'slug_secondary' => $location->slug_secondary,
            'children_count' => $location->children_count,
        ];
    }
//    var_dump(round(mb_strlen(serialize($params), '8bit') / 1024 / 1024, 2));
    $responses = $elastic->bulk($params);
    if ($responses['errors']) {
        throw new \Exception('Error while indexing documents');
    }
    unset($responses);
}

printf('Done indexing %d documents %s', count($locations), PHP_EOL);
printf('Total time: %ss %s', round(microtime(true) - $start, 2), PHP_EOL);
printf('Peak memory: %sMB %s', round(memory_get_peak_usage(true) / 1024 / 1024), PHP_EOL);

/*
 * Make coordinates mandatory for city level
 *
 * UPDATE
    pf_location l1
    JOIN pf_location l2
        ON l1.map_to = l2.id
        AND l1.level = 0 SET l2.coordinates_lat = l1.coordinates_lat,
    l2.coordinates_lon = l1.coordinates_lon ;
 */
